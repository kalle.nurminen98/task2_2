## Table of Contents

- [Sections](#sections)
  - [Title](#title)
  - [Short Description](#short-description)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Sections

### Title

Docker test

### Short Description

Testing docker

### Usage

$docker build https://gitlab.com/kalle.nurminen98/task2_2.git\#main -t docker-test

$docker run --rm -v $(pwd):/usr/src/ -w /usr/src/ gcc/build

$./build/main

### Unique use of docker

Configuration with consistency because you can put your configurations into code and deploy it. For example environment is same for every developer in a team and they can use it and not forced to make new environment for coding.

### Docker daemon

There is thing called docker daemon which listens docker API requests and manages docker objects such as images, containers, networks and volumes.

### Maintainer(s)

Kalle Nurminen @kalle.nurminen98

### Contributing

Kalle Nurminen @kalle.nurminen98

### License

Free to use. No license
