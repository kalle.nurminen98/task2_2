FROM gcc:11

WORKDIR /app

RUN ls -al
RUN apt-get update --yes
RUN apt install --yes cmake
COPY ./build.sh ./build.sh
ENTRYPOINT bash ./build.sh



